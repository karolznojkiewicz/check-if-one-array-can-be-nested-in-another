(function () {
    const canNest = (arrA, arrB) => {
        Math.min(...arrA) > Math.min(...arrB);
        Math.max(...arrA) < Math.max(...arrB);
    };
    canNest([1, 2, 3, 4], [0, 5]);
})();
